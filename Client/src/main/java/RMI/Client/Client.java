package RMI.Client;

import java.rmi.registry.LocateRegistry; 
import java.rmi.registry.Registry;

import javafx.application.Application;
import RMI.Interface.Controller;
import RMI.Interface.View;
import RMI.entities.Car;
import RMI.serviceinterfaces.ICarsService;
import RMI.serviceinterfaces.ISellingService;
import RMI.serviceinterfaces.ITaxService;  

public class Client {  
   private Client() {}  
   public static void main(String[] args) {  
	  String host = (args.length < 1) ? null : args[0];
	  
	  View v = new View();
	  //Application.launch(View.class, args);
      try {  
    	  System.out.println("aaaaaaa");
         // Getting the registry 
         Registry registry = LocateRegistry.getRegistry(null); 
    
         ISellingService sellStub = (ISellingService) registry.lookup("ISellingService"); 
         ITaxService taxStub = (ITaxService) registry.lookup("ITaxService"); 
         
         Controller cont = new Controller(v, sellStub, taxStub);
         cont.startView();
         View.launch(View.class);
         //Application.launch(View.class, args);
         /*Car c = new Car(2009, 23000, 15550);
         
         // Calling the remote method using the obtained object 
         double value = taxStub.computeTax(c); 
         double value1 = sellStub.computeSellingPrice(c);
         
         System.out.print("Tax value : " + value);
         System.out.print("\nSelling value : " + value1);*/
 
      } catch (Exception e) {
         System.err.println("Client exception: " + e.toString()); 
         e.printStackTrace(); 
      } 
   } 
}
