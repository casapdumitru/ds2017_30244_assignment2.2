package RMI.Interface;

import RMI.entities.Car;
import RMI.serviceinterfaces.ISellingService;
import RMI.serviceinterfaces.ITaxService;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class Controller {
	
	private View view;
	private ISellingService selStub;
	private ITaxService taxStub;
	
	public Controller(View view, ISellingService selStub, ITaxService taxStub) {
		this.view = view;
		this.selStub = selStub;
		this.taxStub = taxStub;
	}
	
	EventHandler<ActionEvent> buttonHandler = new EventHandler<ActionEvent>() {
	    public void handle(ActionEvent event) {
	    	
	    	String option = view.comboBox.getSelectionModel().getSelectedItem().toString();
	    	
	    	try {
	    		int year = Integer.parseInt(view.inputYear.getText());
		    	int engineCapacity = Integer.parseInt(view.inputEngineCapacity.getText());
		    	double purchasingPrice = Double.parseDouble(view.inputPurchasingPrice.getText());
		    	Car car = new Car(year, engineCapacity, purchasingPrice);
		    	if(option.equals("Tax")) {
		    		
		    	} else if(option.equals("Selling Price")) {
		    		
		    	}
	    	} catch(Exception e) {
	    		e.printStackTrace();
	    	}
	    	
	    	
	        System.out.println(view.comboBox.getSelectionModel().getSelectedItem().toString());
	    }
	};
	
	public void startView() {
		view.setEventHandler(buttonHandler);;
	}
}
