package RMI.Interface;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


import RMI.entities.Car;
import RMI.serviceinterfaces.ISellingService;
import RMI.serviceinterfaces.ITaxService;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets; 
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class View extends Application {
	
	private static ISellingService sellStub;
	private static ITaxService taxStub;
	
	TextField inputYear;
	TextField inputEngineCapacity;
	TextField inputPurchasingPrice;
	Text result;
	ComboBox comboBox;
	Button buttonTax;
	
	EventHandler<ActionEvent> buttonHandler = new EventHandler<ActionEvent>() {
	    public void handle(ActionEvent event) {
	    	
	    	String option = comboBox.getSelectionModel().getSelectedItem().toString();
	    	
	    	try {
	    		int year = Integer.parseInt(inputYear.getText());
		    	int engineCapacity = Integer.parseInt(inputEngineCapacity.getText());
		    	double purchasingPrice = Double.parseDouble(inputPurchasingPrice.getText());
		    	Car car = new Car(year, engineCapacity, purchasingPrice);
		    	if(option.equals("Tax")) {
		    		double tax = taxStub.computeTax(car);
		    		result.setText("The tax is " + tax);
		    	} else if(option.equals("Selling Price")) {
		    		double price = sellStub.computeSellingPrice(car);
		    		result.setText("The selling price is " + price);
		    	}
	    	} catch(Exception e) {
	    		e.printStackTrace();
	    		result.setText("The introduced values are wrong");
	    	}
	    }
	};
	
	 @Override 
	   public void start(Stage stage) { 
	      //Creating a line object 
	      
		  Double d = Double.parseDouble("12");
		  System.out.println(d);
	      Group root = new Group(); 
	      
	      Text labelYear = new Text();  
	      labelYear.setX(185); 
	      labelYear.setY(60);          
	      labelYear.setText("Year:"); 
	      labelYear.setStyle("-fx-font: 24 arial;");
	      
	      inputYear = new TextField ();
	      inputYear.setLayoutX(260);
	      inputYear.setLayoutY(40);
	    		  
	    
	      Text engineCapacity = new Text();  
	      engineCapacity.setX(60); 
	      engineCapacity.setY(120);          
	      engineCapacity.setText("Engine Capacity:");
	      engineCapacity.setStyle("-fx-font: 24 arial;");
	      
	      inputEngineCapacity = new TextField ();
	      inputEngineCapacity.setLayoutX(260);
	      inputEngineCapacity.setLayoutY(100);
	      
	      Text purchasingPrice = new Text();  
	      purchasingPrice.setX(52); 
	      purchasingPrice.setY(180);          
	      purchasingPrice.setText("Purchasing Price:");
	      purchasingPrice.setStyle("-fx-font: 24 arial;");
	      
	      inputPurchasingPrice = new TextField ();
	      inputPurchasingPrice.setLayoutX(260);
	      inputPurchasingPrice.setLayoutY(160);
	      
	      ObservableList<String> options = 
	    		    FXCollections.observableArrayList(
	    		        "Tax",
	    		        "Selling Price"
	    		    );
	      
	      comboBox = new ComboBox(options);
	      comboBox.getSelectionModel().selectFirst();
	      
	      buttonTax = new Button("Compute");
	      buttonTax.setStyle("-fx-border-color: #ff0000; -fx-border-width: 5px;");
	      buttonTax.setStyle("-fx-background-color: #00ff00");
	      buttonTax.setStyle("-fx-font-size: 2em; ");
	      buttonTax.setStyle("-fx-text-fill: #0000ff");
	      buttonTax.setOnAction(buttonHandler);
	      
	      HBox hbox = new HBox(comboBox, buttonTax);
	      hbox.setLayoutX(180);
	      hbox.setLayoutY(220);
	      hbox.setPadding(new Insets(0, 10, 10, 10));
	      hbox.setSpacing(40);
	      
	      result = new Text();
	      result.setX(50); 
	      result.setY(275);          
	      result.setText("");
	      result.setStyle("-fx-font: 18 arial;");
	      
	      ObservableList list = root.getChildren(); 
	       
	      //Setting the text object as a node to the group object 
	      list.add(labelYear);
	      list.add(engineCapacity);
	      list.add(purchasingPrice);
	      list.add(inputYear);
	      list.add(inputEngineCapacity);
	      list.add(inputPurchasingPrice);
	      list.add(hbox);
	      list.add(result);
	      //Creating a Group 
	      
	         
	      //Creating a Scene 
	      Scene scene = new Scene(root, 600, 300); 
	      
	      //Setting title to the scene 
	      stage.setTitle("Cars application"); 
	         
	      //Adding the scene to the stage 
	      stage.setScene(scene); 
	         
	      //Displaying the contents of a scene 
	      stage.show(); 
	   }      
	   public static void main(String args[]){ 
	     
	      try {  
	         // Getting the registry 
	         Registry registry = LocateRegistry.getRegistry(null); 
	    
	         sellStub = (ISellingService) registry.lookup("ISellingService"); 
	         taxStub = (ITaxService) registry.lookup("ITaxService"); 

	         /*Car c = new Car(2009, 23000, 15550);
	         
	         // Calling the remote method using the obtained object 
	         double value = taxStub.computeTax(c); 
	         double value1 = sellStub.computeSellingPrice(c);
	         
	         System.out.print("Tax value : " + value);
	         System.out.print("\nSelling value : " + value1);*/
	 
	      } catch (Exception e) {
	         System.err.println("Client exception: " + e.toString()); 
	         e.printStackTrace(); 
	      } 
	      
	      launch(args); 
	   }
	   
	   public void setEventHandler(EventHandler<ActionEvent> ev) {
		   this.buttonTax.setOnAction(ev);
	   }
	

}
