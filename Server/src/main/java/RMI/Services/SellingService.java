package RMI.Services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import RMI.entities.Car;
import RMI.serviceinterfaces.ISellingService;

public class SellingService /*extends UnicastRemoteObject*/ implements ISellingService {
	
	public SellingService() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public double computeSellingPrice(Car c) {
		
		if (c.getEngineCapacity() <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		if (c.getPurchasingPrice() < 0) {
			throw new IllegalArgumentException("Purchasing price must be positive.");
		}
		
		double priceSelling = c.getPurchasingPrice()- (c.getPurchasingPrice() / 7 * (2015 - c.getYear()));
		return priceSelling;
	}
}