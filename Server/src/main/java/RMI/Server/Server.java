package RMI.Server;

import java.rmi.registry.Registry; 
import java.rmi.registry.LocateRegistry;
import java.rmi.Remote;
import java.rmi.RemoteException; 
import java.rmi.server.UnicastRemoteObject;

import RMI.Services.SellingService;
import RMI.Services.TaxService;
import RMI.entities.Car;
import RMI.serviceinterfaces.ICarsService;
import RMI.serviceinterfaces.ISellingService;
import RMI.serviceinterfaces.ITaxService; 

public class Server{ 
	
   public Server() {} 
   
   public static void main(String args[]) { 
      try { 
    	  Server obj = new Server();
    	    
    	  TaxService taxService = new TaxService();
    	  SellingService selService = new SellingService();
    	  
          ITaxService taxStub = (ITaxService) UnicastRemoteObject.exportObject( taxService, 0);
          ISellingService sellingStub = (ISellingService) UnicastRemoteObject.exportObject( selService, 0);
          
          // Bind the remote object's stub in the registry
          Registry registry = LocateRegistry.createRegistry(1099);
          
          registry.bind("ITaxService", taxStub);
          registry.bind("ISellingService", sellingStub);
          
          /*registry.bind("ITaxService", taxService);
          registry.bind("ISellingService", selService);*/
          
          System.err.println("Server ready");
          
      } catch (Exception e) { 
         System.err.println("Server exception: " + e.toString()); 
         e.printStackTrace(); 
      } 
   }

 
}