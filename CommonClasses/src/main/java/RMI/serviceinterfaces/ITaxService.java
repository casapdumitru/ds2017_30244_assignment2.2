package RMI.serviceinterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import RMI.entities.Car;

public interface ITaxService extends Remote {
	
	double computeTax(Car c) throws RemoteException;
}
