package RMI.serviceinterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import RMI.entities.Car;

public interface ICarsService extends Remote {

	double computeSellingPrice(Car c) throws RemoteException;
		
	double computeTax(Car c) throws RemoteException;

}
