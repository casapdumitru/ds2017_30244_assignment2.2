package RMI.serviceinterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import RMI.entities.Car;

public interface ISellingService extends Remote {
	
	double computeSellingPrice(Car c) throws RemoteException;
} 
